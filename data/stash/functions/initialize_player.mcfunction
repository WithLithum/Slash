# Must be executed on player!

tellraw @s "即将设置 Stash 游戏环境出生点。"
tellraw @s "此操作必须由一名管理员玩家执行。现在将开始。"

team add players "玩家"
team add admin "管理员"
team modify admin color red
team modify admin prefix ""

setblock ~ ~-1 ~ stone
setblock ~1 ~-1 ~ command_block{Command:'title @p title {"text": "The Stash","bold": true,"color": "green"}'}
setblock ~ ~-1 ~1 command_block{Command:"effect give @p regeneration"}
setblock ~ ~-1 ~-1 command_block{Command:"effect give @p glowing 5"}
setblock ~-1 ~-1 ~ command_block{Command:"team join players @p[team=!players,team=!admin]"}

team join admin

gamerule commandBlockOutput false
gamerule keepInventory true
gamerule doInsomnia false
gamerule spawnRadius 1

setworldspawn
worldborder center ~ ~
worldborder set 10000

tellraw @s "玩家设置完毕。请前往另一地方开始设置环境。"